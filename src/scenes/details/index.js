import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { FaTrash } from 'react-icons/fa';

import './index.css';

import { removeMatches } from '../../creators';

const Details = (props) => {
  const { match: { params }, matches, onRemoveMatches } = props;

  const match = matches.find(m => m.id === params.id);

  return (!match) ?
    <Redirect to='/details' /> :
    (
      <React.Fragment>
        <Container className='paddingTop'>
          <Row>
            <Col>
              <div className='match-data'>
                <ul className='noBullets'>
                  <li>
                    <b>SAP File Name:</b> {match.sapFilename}
                  </li>
                  <li>
                    <b>ODM (EAP or OAP) File Name:</b> {match.odmFilename}
                  </li>
                  <li>
                    <b>Matches:</b> {match.references.length}/{match.references.length + match.referencesNotFound.length}
                  </li>
                </ul>
              </div>
              <div className='width15'>
                <Button variant='danger' block onClick={() => onRemoveMatches(match.id)}>
                  <FaTrash />
                </Button>
              </div>
            </Col>
            <Col>
              <div>
                <h3>References found <b>({match.references.length})</b></h3>
                <ul className='referenceList'>
                  {
                    match.references.map((r, i) => <li key={i}>{r}</li>)
                  }
                </ul>
              </div>
              <div>
                <h3>References not found <b>({match.referencesNotFound.length})</b></h3>
                <ul className='referenceList'>
                  {
                    match.referencesNotFound.map((r, i) => <li key={i}>{r}</li>)
                  }
                </ul>
              </div>
            </Col>
          </Row>
        </Container>
      </React.Fragment>
    );

};

const mapStateToProps = state =>
  ({
    matches: [...state.matches],
  });

const mapDispatchToProps = dispatch =>
  ({
    onRemoveMatches: id => dispatch(removeMatches(id)),
  });

export default connect(mapStateToProps, mapDispatchToProps)(Details);
