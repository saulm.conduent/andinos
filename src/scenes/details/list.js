import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Container, Table, Button } from 'react-bootstrap';
import { FaTrash, FaBars } from 'react-icons/fa';

import './index.css';

import { removeMatches } from '../../creators';

const DetailsList = (props) => {
  const { matches, onRemoveMatches } = props;

  return (
    <React.Fragment>
      <Container className='paddingTop'>
        <Table responsive striped bordered hover size='sm'>
          <thead>
            <tr>
              <th>SAP File Name</th>
              <th>ODM (EAP or OAP) File Name</th>
              <th>References</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {
              matches.map((m, i) => (
                <tr key={i}>
                  <td>{m.sapFilename}</td>
                  <td>{m.odmFilename}</td>
                  <td>{m.references.length}/{m.references.length + m.referencesNotFound.length}</td>
                  <td className='tdWidth'>
                    <Link to={`/details/${m.id}`}>
                      <Button variant='info'>
                        <FaBars />
                      </Button>
                    </Link>
                    <Button variant='danger' onClick={() => onRemoveMatches(m.id)}>
                      <FaTrash />
                    </Button>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </Table>
      </Container>
    </React.Fragment>
  );
};

const mapStateToProps = state =>
  ({
    matches: [...state.matches].reverse(),
  });

const mapDispatchToProps = dispatch =>
  ({
    onRemoveMatches: id => dispatch(removeMatches(id)),
  });

export default connect(mapStateToProps, mapDispatchToProps)(DetailsList);
