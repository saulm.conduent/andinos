import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Form, Button } from 'react-bootstrap';
import { FaTrash } from 'react-icons/fa';

const FilesUpload = ({
  clearSapInput,
  handleSapFile,
  handleOdmFile,
  isSapRequired,
}) => (
    <Form>
      <Row>
        <Col sm={1} className='align-self-center'>
          <Button variant='danger' onClick={clearSapInput} className='align-center' block>
            <FaTrash />
          </Button>
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>SAP Excel File</Form.Label>
            <Form.Control type="file" onChange={handleSapFile} id='sapFileInputId' disabled={!isSapRequired} />
          </Form.Group>
        </Col>
        <Col>
          <Form.Group>
            <Form.Label>ODM (EAP or OAP) Excel File</Form.Label>
            <Form.Control type="file" onChange={handleOdmFile} id='odmFileInputId' disabled={isSapRequired} />
          </Form.Group>
        </Col>
      </Row>
    </Form>
  );

FilesUpload.propTypes = {
  clearSapInput: PropTypes.func,
  handleSapFile: PropTypes.func,
  handleOdmFile: PropTypes.func,
  isSapRequired: PropTypes.bool,
};

FilesUpload.defaultProps = {
  clearSapInput: f => f,
  handleSapFile: f => f,
  handleOdmFile: f => f,
  isSapRequired: true,
};

export default FilesUpload;
