import React from 'react';
import { Link } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

const RecentCardMatch = ({
  id,
  timestamp,
  sapFilename,
  odmFilename,
  references,
  referencesNotFound,
}) => (
    <React.Fragment>
      <Card>
        <Card.Body>
          <Card.Title>{references.length} match(es) found!</Card.Title>
          <Card.Text>
            We have found {references.length} of {references.length + referencesNotFound.length} reference(s)
            between <b>{sapFilename}</b> and <b>{odmFilename}</b>
          </Card.Text>
          <Link to={`/details/${id}`}>
            <Button variant='info'>Info</Button>
          </Link>
        </Card.Body>
        <Card.Footer>
          <small className='text-muted'>{new Date(timestamp).toLocaleDateString()}</small>
        </Card.Footer>
      </Card>
    </React.Fragment>
  );

RecentCardMatch.propTypes = {
  id: PropTypes.string,
  timestamp: PropTypes.number,
  sapFilename: PropTypes.string,
  odmFilename: PropTypes.string,
  references: PropTypes.array,
  referencesNotFound: PropTypes.array,
};

RecentCardMatch.defaultProps = {
  id: 'ayyy',
  timestamp: Date.now(),
  sapFilename: '',
  odmFilename: '',
  references: [],
  referencesNotFound: [],
};

export default RecentCardMatch;
