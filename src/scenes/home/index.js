import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Container, Row, CardDeck } from 'react-bootstrap';
import readXlsxFile from 'read-excel-file';
import LoadingOverlay from 'react-loading-overlay';

import './index.css';

import { addMatches } from '../../creators';

import RecentMatchCard from './recent-match-card';
import FilesUpload from './files-upload';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sap: {
        filename: '',
        references: [],
      },
      odm: {
        filename: '',
        references: [],
      },
      isLoading: false,
    };

    this.toggleLoading = this.toggleLoading.bind(this);
    this.handleSapFile = this.handleSapFile.bind(this);
    this.handleOdmFile = this.handleOdmFile.bind(this);
    this.clearSapInput = this.clearSapInput.bind(this);
    this.clearOdmInput = this.clearOdmInput.bind(this);
    this.matchReferences = this.matchReferences.bind(this);
  }

  toggleLoading() { this.setState({ isLoading: !this.state.isLoading }); }

  async handleReadXlsx(file, initialRow = 0, column = 0) {
    const rows = await readXlsxFile(file);

    const output = rows.slice(initialRow)
      .map((item) => {
        const reference = `${item[column]}`;
        return reference
          .replace(/ /g, '')
          .replace(/-/g, '')
          .replace(/_/g, '')
          .replace(/\./g, '')
          .replace(/\//g, '')
          .replace(/\\/g, '');
      })
      .filter((item) => (item && item.length > 0 && item !== 'null'));

    return output;
  }

  async handleSapFile(e) {
    try {
      const [file] = e.target.files;
      const [extension] = file.name.toLowerCase().split('.').reverse();

      if (!['xls', 'xlsx'].some(ext => ext === extension)) {
        this.clearSapInput();
        throw new Error('Can only upload Excel file');
      }

      const output = await this.handleReadXlsx(file, 9, 6);

      this.clearOdmInput();
      this.setState({
        sap: {
          filename: file.name,
          references: output,
        },
      });
    } catch (error) {
      console.error(error);
    }
  }

  async handleOdmFile(e) {
    this.toggleLoading();
    try {
      const [file] = e.target.files;
      const [extension] = file.name.toLowerCase().split('.').reverse();

      if (!['xls', 'xlsx'].some(ext => ext === extension)) {
        this.clearOdmInput();
        throw new Error('Can only upload Excel file');
      }

      const output = await this.handleReadXlsx(file, 3, 10);

      this.setState({
        odm: {
          filename: file.name,
          references: output,
        },
      });

      this.matchReferences();
    } catch (error) {
      console.error(error);
    }
    this.toggleLoading();
  }

  clearSapInput() {
    const element = document.getElementById('sapFileInputId');

    this.clearOdmInput();
    element.value = null;
    this.setState({
      sap: {
        filename: '',
        references: [],
      },
    });
  }

  clearOdmInput() {
    const element = document.getElementById('odmFileInputId');

    element.value = null;
    this.setState({
      odm: {
        filename: '',
        references: [],
      },
    });
  }

  matchReferences() {
    const {
      sap,
      odm,
    } = this.state;

    const references = [...sap.references].filter(r => odm.references.includes(r));
    const referencesNotFound = [...sap.references].filter(r => !references.includes(r));

    this.props.onNewMatches({
      sapFilename: sap.filename,
      odmFilename: odm.filename,
      references,
      referencesNotFound,
    });
  }

  render() {
    const { sap: { references: sapReferences }, isLoading } = this.state;
    const { recentMatches } = this.props;

    return (
      <React.Fragment>
        <LoadingOverlay
          active={isLoading}
          spinner
          text='Loading...'>
          <Container >
            <div className='paddingTop'>
              <FilesUpload
                clearSapInput={this.clearSapInput}
                handleSapFile={this.handleSapFile}
                handleOdmFile={this.handleOdmFile}
                isSapRequired={(sapReferences.length === 0)} />
            </div>
            <div className='paddingTop'>
              <Row>
                <CardDeck>
                  {
                    recentMatches.map((data, i) => <RecentMatchCard key={i} {...data} />)
                  }
                </CardDeck>
              </Row>
            </div>
          </Container>
        </LoadingOverlay>
      </React.Fragment >
    );
  }
}

const mapStateToProps = state =>
  ({
    // matches: [...state.matches],
    recentMatches: [...state.matches].reverse().filter((_, i) => i <= 2),
  });

const mapDispatchToProps = dispatch =>
  ({
    onNewMatches: (matches) => {
      dispatch(addMatches(matches));
    },
  });

export default connect(mapStateToProps, mapDispatchToProps)(Home);
