import { createStore, combineReducers, applyMiddleware } from 'redux';

import { matches } from '../reducers';

const lsName = 'redux-store-andinos';

const saver = store => next => action => {
  let result = next(action);

  localStorage[lsName] = JSON.stringify(store.getState());

  return result;
};

const middleware = [saver];

const storeFactory = (initialState = {}) =>
  applyMiddleware(...middleware)(createStore)(
    combineReducers({ matches }),
    (localStorage[lsName]) ?
      JSON.parse(localStorage[lsName]) :
      initialState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  );

export default storeFactory;