import { v4 } from 'uuid';
import ActionConstants from '../constants';

export const addMatches = ({
  sapFilename,
  odmFilename,
  references,
  referencesNotFound,
}) =>
  ({
    type: ActionConstants.ADD_MATCHES,
    id: v4(),
    timestamp: Date.now(),
    sapFilename,
    odmFilename,
    references,
    referencesNotFound,
  });

export const removeMatches = id => ({ type: ActionConstants.REMOVE_MATCHES, id });
