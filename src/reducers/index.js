import ActionConstants from '../constants';

export const matches = (state = [], action) => {
  switch (action.type) {
    case ActionConstants.ADD_MATCHES: {
      return [
        ...state,
        {
          id: action.id,
          timestamp: action.timestamp,
          sapFilename: action.sapFilename,
          odmFilename: action.odmFilename,
          references: action.references,
          referencesNotFound: action.referencesNotFound,
        },
      ];
    }

    case ActionConstants.REMOVE_MATCHES: {
      return state.filter(m => m.id !== action.id);
    }

    default: { return state; }
  }
};
