import React from 'react';
import { Alert as BAlert } from 'react-bootstrap';
import PropTypes from 'prop-types';

const Alert = ({ alert, removeAlert }) => {
    const {
        variant,
        uuid,
        title,
        message,
    } = alert;

    return (
        <BAlert variant={variant} onClose={removeAlert(uuid)}>
            <BAlert.Heading>{title}</BAlert.Heading>
            <p>{message}</p>
        </BAlert>
    );
};

Alert.propTypes = {
    alert: PropTypes.shape({
        variant: PropTypes.string,
        uuid: PropTypes.string,
        title: PropTypes.string,
        message: PropTypes.string,
    }),
    removeAlert: PropTypes.func,
};

Alert.defaultProps = {
    alert: {},
    removeAlert: f => f,
};

export default Alert;
