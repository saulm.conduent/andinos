import React from 'react';
import uuidv4 from 'uuid/v4';

import Alert from './index';

const AlertsList = () => {
  const removeAlert = (uuid) => console.log(uuid);

  const alerts = [
    // {
    //   variant: 'primary',
    //   uuid: uuidv4(),
    //   title: 'Alert',
    //   message: 'Message',
    // },
  ];

  return (
    <React.Fragment>
      <ul className='alert-list'>
        {
          alerts.slice(0).reverse().map((alert, i) =>
            (i > 2) ? null :
              <Alert key={alert.uuid} alert={alert} removeAlert={removeAlert} />
          )
        }
      </ul>
    </React.Fragment>
  );
};

export default AlertsList;
