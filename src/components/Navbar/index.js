import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar as BNavbar, Nav } from 'react-bootstrap';


const Navbar = () => (
    <React.Fragment>
        <BNavbar bg='dark' variant='dark'>
            <BNavbar.Brand></BNavbar.Brand>
            <Nav className="mr-auto">
                <Nav.Link as={Link} to='/'>Home</Nav.Link>
                <Nav.Link as={Link} to='/details'>Details</Nav.Link>
            </Nav>
        </BNavbar>
    </React.Fragment>
);

export default Navbar;
