import React from 'react';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import storeFactory from './store';

import Navbar from './components/Navbar';
// import AlertList from './components/Alert/list';

import Home from './scenes/home';
import DetailsList from './scenes/details/list';
import Details from './scenes/details';

const store = storeFactory();

const App = () => (
  <Provider store={store}>
    <Router>
      <div className='app'>
        <Navbar />
        <div className='app-switch'>
          {/* <AlertList /> */}
          <div className='app-page-content'>
            <Switch>
              <Route exact path='/' component={Home} />
              <Route exact path='/details' component={DetailsList} />
              <Route path='/details/:id' component={Details} />
            </Switch>
          </div>
        </div>
      </div>
    </Router>
  </Provider>
);

export default App;
